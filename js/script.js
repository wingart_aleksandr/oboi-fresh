//----Include----
    if($(".owl_carousel").length){
      include("js/owl.carousel.js");
    }
    if($(".flexslider").length){
      include("js/jquery.flexslider.js");
    }
    if($(".data-mh").length){
      include("js/jquery.matchHeight-min.js");
    }
    if($(".open_popup").length){
      include("js/jquery.arcticmodal.js"); 
    }
    if($(".masked").length){
      include("js/maskedInput.js"); 
    };
    if($(".scroll-pane").length){
      include("js/jquery.jscrollpane.js"); 
    };
    if($(".scroll-mouse").length){
      include("js/jquery.mousewheel.js"); 
    };
    if($(".rangeslider").length){
      include("js/ion.rangeSlider.min.js"); 
    };

//----Include-Function----
    function include(url){
      document.write('<script src="'+ url + '"></script>');
    }
//----Include end----

 $(document).ready(function(){
/*------Cкролл--------*/
    if ($('.scroll-mouse').length){
      $('.scroll-mouse').mousewheel();  
    };

    if ($('.scroll-pane').length){
      if($('.scroll-pane').hasClass('arr_top_down')){
        $('.scroll-pane').jScrollPane({
          showArrows: true
        });
      }
      else{
        $('.scroll-pane').jScrollPane();
      }
    }; 

    /*-----Активность цвета (фильтр)------*/
    $(".selection_filter_color_list_item").on('click', function(){
      $(this).toggleClass("active");
    });
    /*-----Выпадашка------*/

    if($('.accordion_item_link').length){
      $('.accordion_item_link').on('click', function(){
        $(this)
          .toggleClass('active')
          .next('.sub-menu')
          .slideToggle()
          .parents(".accordion_item")
          .siblings(".accordion_item")
          .find(".accordion_item_link")
          .removeClass("active")
          .next(".sub-menu")
          .slideUp();
      });  
    }

    // RangeSlider start
    
    if($('#range1').length){
        $("#range1").ionRangeSlider({
          type: "double",
          min: 4500,
          max: 12500,
          from: 4500,
          to: 12500,
          hide_min_max: true,
          hide_from_to: false,
          grid: false
        });
    }
    // RangeSlider end

    // Перенос картинки и изменение картинки в слайдере  start

    if($(".draggable").length){
        $( ".draggable" ).draggable({
            revert: true,
            start: function(event, ui){
                $(ui.helper.context).addClass('dragging');
            },
            stop: function(event, ui){
                $(ui.helper.context).removeClass('dragging');
            }
        });    
    };
    if($(".droppable").length){
        $( ".droppable" ).droppable({
          drop: function( event, ui ) {

            var data = $(ui.helper.context).data('img-src');

            $(this).children('img').attr('src', data);

            // $( this ).addClass( "ui-state-highlight" );
            console.log(data);
          }
        });
    };
// ==========================end===============================

//--------------masked input--------
    if($(".masked").length){

        $(function($){
          if( $("#date").length || $("#phone").length){
            $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
            $("#phone").mask("+7-***-***-**-**");
          }
        });

    };

  /*----flexslider-----*/


if($('#carousel').length){

  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 90,
    itemMargin: 10,
    touch: true,
    asNavFor: '#slider'
  });

};  

if($('#slider').length){

  if($('#slider').hasClass('navigation_box')){

      $('#slider').flexslider({
          animation: "slide",
          controlNav: true,
          startAt: 0,
          slideshow: true,
          slideshowSpeed: 2000, 
          animationLoop: true,          
          touch: true,
          pauseOnHover: true,
          mousewheel: false,
          sync: "#carousel"
      });

  }
  else{

      $('#slider').flexslider({
          animation: "slide",
          controlNav: false,
          animationLoop: false,
          slideshow: false,
          touch: true,
          sync: "#carousel"
      });

  }

};

  //----owl-carousel-----
  if($('.owl_carousel').length){
    $('.owl_carousel').each(function(){
      var $this = $(this);
      if($this.hasClass('full_width')){
        $(this).owlCarousel({
          stopOnHover : true,
          rewindNav:false,
          navigation:true,
          paginationSpeed : 1000,
          goToFirstSpeed : 1000,
          singleItem : true,
          items : 1,
          loop: true,
          smartSpeed:1000,
          mouseDrag:true,
          // autoplay:true,
          autoplayTimeout:4000,
        });
      }
      else{
        var itemA = $(this).attr('data-itemA'),
            itemB = $(this).attr('data-itemB'),
            itemC = $(this).attr('data-itemC'),
            itemD = $(this).attr('data-itemD'),
            itemE = $(this).attr('data-itemE');
            itemF = $(this).attr('data-itemF');
            itemG = $(this).attr('data-itemG');
        $(this).owlCarousel({
            // margin: 10 ,
            nav: true ,
            mouseDrag:true,
            paginationSpeed : 1000,
            goToFirstSpeed : 1000,
            smartSpeed:500,
            responsive: {
                0 : {
                     items: itemG
                },
                440 : {
                     items: itemF
                },
                620 : {
                     items: itemE
                },
                768 : {
                     items: itemD
                },
                991 : {
                     items: itemC
                },
                1199 : {
                     items: itemB
                },
                1900 : {
                     items: itemA
                }
            }
        });
      }
    });
  };

    // popup

        if($(".open_popup").length){
          $(".open_popup").on('click',function(){
              var modal = $(this).data("modal");
              $(modal).arcticmodal();
          });
        };
      
    // popup end

});


 $(document).ready(function(){

    //  counter basket

      (function(){
        function getSpacePosition(str){

          if(str.length > 6) return str;

          var result = "",
            firstPart = "",
            secondPart,
            currPos;

          for(var j = str.length - 3; j > 0; j -= 3){

            firstPart += str.slice(0, j) + " ";
            secondPart = str.slice(j);

          }

          return firstPart + secondPart;
        }

        $('.counter_number').each(function(){

          var $this = $(this),
            input = $this.children('input'),
            output = $this.closest('div').find('.total_cost');  //,'li div span'
            output.data('price', parseFloat(output.text().split(" ").join(""),10));

            // console.log(output.data('price'));

            $this.on('click','button', function(){

              var currentVal = +input.val(),
              cO = parseFloat(output.text().split(" ").join(""),10);
              direction = $(this).hasClass('counter_minus') ? 'minus' : 'plus';

              if( +input.val() - 1 == 0 && direction == "minus") return;
       
              if(direction == "plus"){
                input.val(++currentVal); 
                output.text(getSpacePosition(currentVal * output.data('price') + ""));
              }
              else{
                input.val(--currentVal); 
                output.text(getSpacePosition(cO - output.data('price') + "")); 
              }
            });
          });
      }());


});
